<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link href="css/style.css" rel="stylesheet" type="text/css">
	<title>ログイン</title>
</head>
<body>
	<div class="main">
		<div class="pageName">
			<h1>ログイン</h1>
		</div>

		<!-- エラーメッセージ  -->
		<c:if test="${ not empty errMessages }">
			<div class="err-messages">
				<ul>
					<c:forEach items="${errMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errMessages" scope="session"/>
		</c:if>

		<!-- 入力フォーム -->
		<div class="login-form">
			<form action="login" method="post" ><br />
				<label for="loginId">ログインID</label>
				<input name="loginId" id="loginId" value="${loginId}"/> <br />

				<label for="password">パスワード</label>
				<input name="password" type="password" id="password"/> <br />

				<input type="submit" value="ログイン" /> <br />
			</form>
		</div>

		<div class="copyright">Copyright(c)Hiroki Sanada</div>
	</div>
</body>
</html>