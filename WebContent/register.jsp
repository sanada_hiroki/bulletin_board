<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ユーザー新規登録</title>
	<link href="css/style.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="./js/jquery-3.2.1.min.js"></script>
</head>
<body>
<div class="main">
<div class="pageName"><h1>ユーザー新規登録</h1></div>

<!-- <div class="loginUserInf"> -->
<%-- 	ID:${loginUser.loginId} --%>
<%-- 	名前:${loginUser.name} --%>
<%-- 	支店:${loginUser.branchName} --%>
<%-- 	部署:${loginUser.departmentName} --%>
<!-- </div> -->

<div class="headerLink">
	<div class="right-link"><a href="management">ユーザー管理</a></div>
	<div class="logout-link"><a href="logout">ログアウト</a></div>
</div>

<c:if test="${ not empty errMessages }">
	<div class="err-messages">
		<ul>
			<c:forEach items="${errMessages}" var="message">
				<li><c:out value="${message}" />
			</c:forEach>
		</ul>
	</div>
	<c:remove var="errMessages" scope="session"/>
</c:if>

<div class="user-form">
<form action="register" method="post">
	<label for="loginid">ログインID（半角英数字6文字以上20文字以下）</label>
	<input name="loginid" id="loginid" value="${user.loginId}"/>
	<label for="name">名前（10文字以下）</label>
	<input name="name" id="name" value="${user.name}"/>
	<label for="password">パスワード(記号を含む全ての半角文字で6文字以上20文字以下)</label>
	<input name="password" type="password" id="password"/>
	<label for="checkPassword">確認用パスワード</label>
	<input name="checkPassword" type="password" id="checkPassword"/>
	<label for="branch">支店</label>
	<select name="branch">
		<c:forEach var="branch" varStatus="s" items="${branches}">
			<c:choose>
				<c:when test="${branch.id==user.branchId}">
					<option value="${branch.id}" SELECTED>${branch.name}</option>
				</c:when>
				<c:otherwise>
					<option value="${branch.id}">${branch.name}</option>
				</c:otherwise>
			</c:choose>
		</c:forEach>
	 </select>
	<label for="department">部署・役職</label>
	<select name="department">
		<c:forEach var="department" varStatus="s" items="${departments }">
			<c:choose>
				<c:when test="${department.id==user.departmentId}">
					<option value="${department.id}" SELECTED>${department.name}</option>
				</c:when>
				<c:otherwise>
					<option value="${department.id}">${department.name}</option>
				</c:otherwise>
			</c:choose>
		</c:forEach>
	</select><br>
	<input type="submit" value="登録" id="submit"/> <br />
</form>
</div>
<div class="copyright">Copyright(c)Hiroki Sanada</div>
</div>
<script type="text/javascript">
$("form").submit(function() {
	  var self = this;
	  $(":submit", self).prop("disabled", true);
	  setTimeout(function() {
	    $(":submit", self).prop("disabled", false);
	  }, 10000);
	});
</script>
</body>
</html>
