<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="css/style.css" rel="stylesheet" type="text/css">
<title>ユーザー管理画面</title>
</head>
<body>
<div class="main">
<div class="pageName"><h1>ユーザー管理</h1></div>

<div class="headerLink">
	<div class="right-link">
		<a href="./">ホーム</a>
		<a href="register">新規登録</a>
	</div>
	<div class="logout-link">
		<a href="logout">ログアウト</a>
	</div>
</div>

<c:if test="${ not empty errMessages }">
	<div class="err-messages">
		<ul>
			<c:forEach items="${errMessages}" var="message">
				<li><c:out value="${message}" />
			</c:forEach>
		</ul>
	</div>
	<c:remove var="errMessages" scope="session"/>
</c:if>

<div class="users">
	<table>
		<tr>
		<th>ログインID</th><th>名前</th><th>支店</th><th>部署・役職</th><th>停止・復活</th>
		</tr>
	<c:forEach items="${users}" var="user">
		<tr>
			<td><a href="edit?id=${user.id}">${user.loginId}</a></td>
			<td>${user.name}</td>
			<td>${user.branchName }</td>
			<td>${user.departmentName }</td>
			<td>
			<div class="isBanned">
			<form action="authority" method="post">
				<input type="hidden" name="id" value="${user.id}">
				<c:choose>
					<c:when test="${user.loginId==loginUser.loginId}">
						<button type="submit" id="logined" disabled>------</button>
					</c:when>
					<c:otherwise>
						<c:if test="${user.isBanned==1}">
  							<button type="submit" name="action" value="false" id="banned"onClick="return confirm('このユーザーを復活しますか？')">復活</button>
  						</c:if>
  						<c:if test="${user.isBanned==0}">
  							<button type="submit" name="action" value="true"  id="not-banned" onClick="return confirm('このユーザーを停止しますか？')">停止</button>
  						</c:if>
  					</c:otherwise>
  				</c:choose>
  			</form>
  			</div>
			</td>
		</tr>
	</c:forEach>
	</table>
</div>
<div class="copyright">Copyright(c)Hiroki Sanada</div>
</div>
</body>
</html>