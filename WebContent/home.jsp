<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<script type="text/javascript" src="./js/jquery-3.2.1.min.js"></script>
	<title>掲示板システム-ホーム</title>
	<link href="css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class="main">
<div class="pageName"><h1>掲示板システム</h1></div>

<div class="headerLink">
	<c:if test="${loginUser.branchId == '1'}">
		<div class="right-link">
			<a href="post">新規投稿</a>
			<a href="management">ユーザー管理</a>
		</div>
		<div class="logout-link">
			<a href="logout">ログアウト</a>
		</div>
	</c:if>
	<c:if test="${loginUser.branchId != '1'}">
		<div class="right-link">
			<a href="post">新規投稿</a>
		</div>
		<div class="logout-link">
			<a href="logout">ログアウト</a>
		</div>
	</c:if>
</div>

<c:if test="${ not empty errMessages }">
	<div class="err-messages">
		<ul>
			<c:forEach items="${errMessages}" var="message">
				<li><c:out value="${message}" />
			</c:forEach>
		</ul>
	</div>
	<c:remove var="errMessages" scope="session"/>
</c:if>

<div class="searchForm">
<form action="./" method="get"><br>
	<label for="category">カテゴリー</label>
	<input type="text" name="category" list="category" value="${searchCategory}" autocomplete="off"><br>
	<datalist id="category">
		<c:forEach items="${categorys}" var="categorys" >
			<option value="${categorys}"></option>
		</c:forEach>
	</datalist>
	<p>
	<label for="createdDate">検索期間</label>
	<input type="date" name="startDate" value="${startDate}"> - <input type="date" name="endDate" value="${endDate}"></p>
	<input type="submit" name="search" value="検索"><button type="button"onclick="location.href='./'">リセット</button>
</form>
</div>
<hr>

<c:forEach items="${messages}" var="message">
	<div class="messages">
		<div class="message">
			<div class="message-title"><p>件名：${message.subject}</p></div>
			<div class="message-category"><p>カテゴリー：${message.category}</p></div>
			<div class="message-text"><hr><p>${fn:replace(message.message, retCode, '<br>')}</p><hr></div>
			<span>投稿者：${message.userName}
			投稿日時：<fmt:formatDate value="${message.createdAt}" pattern="yyyy/MM/dd HH:mm:ss" />

			<c:if test="${loginUser.id==message.userId}">
			<form action="delete" method="get">
				<input type="hidden" name="id" value="${message.id}">
				<button type="submit" name="delete" value="true" onClick="return confirm('削除してよろしいですか？')">削除</button>
<!-- 				<input type="submit" name="delete" value="削除"> -->
			</form>
			</c:if>
			</span>
		</div>
		<div class="commentForm">
		<form action="comment" method="post">
			<label>コメント</label>
			<input type="hidden" name="id" value="${message.id}">
			<textarea name="comment" cols=30 rows=3><c:if test="${message.id==mycomment.postId}">${mycomment.message}<c:remove var="mycomment" scope="session"></c:remove></c:if></textarea><input id="submitComment" type="submit" value="送信">
		</form>
		</div>

		<c:forEach items="${comments}" var="comment">
			<c:if test="${message.id==comment.postId}">
				<div class="comments" style="word-break:break-all">
				<form action="comment" method="get">
					<p>${fn:replace(comment.message, retCode, '<br>')}</p><hr>
					<p><span>${comment.userName}&nbsp;&nbsp;&nbsp;<fmt:formatDate value="${comment.createdAt}" pattern="yyyy/MM/dd HH:mm:ss" /></span></p>
					<c:if test="${loginUser.id==comment.userId}">
						<input type="hidden" name="id" value="${comment.id}">
						<button type="submit" name="delete" value="true" onClick="return confirm('削除してよろしいですか？')">削除</button>
					</c:if>
				</form>
				</div><br>
			</c:if>
		</c:forEach>
	</div>
</c:forEach>

<div class="copyright">Copyright(c)Hiroki Sanada</div>
</div>
<script type="text/javascript">
 $("form").submit(function() {
   var self = this;
   $(':submit', self).prop("disabled", true);
   setTimeout(function() {
     $(':submit', self).prop("disabled", false);
   }, 10000);
 });
</script>
</body>
</html>