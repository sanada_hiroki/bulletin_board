<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<script type="text/javascript" src="./js/jquery-3.2.1.min.js"></script>
	<link href="css/style.css" rel="stylesheet" type="text/css">
	<title>新規投稿</title>
</head>
<body>
<div class="main">
<div class="pageName"><h1>新規投稿</h1></div>

<div class="headerLink">
	<div class="right-link"><a href="./">ホーム</a></div>
	<div class="logout-link"><a href="logout">ログアウト</a></div>
</div>

<c:if test="${ not empty errMessages }">
	<div class="err-messages">
		<ul>
			<c:forEach items="${errMessages}" var="message">
				<li><c:out value="${message}" ></c:out></li>
			</c:forEach>
		</ul>
	</div>
	<c:remove var="errMessages" scope="session"/>
</c:if>

<div class="form">
<form action="post" method="post">
	<ul>
		<li class="subject">
			<label for="subject">件名 (30文字以下)</label>
			<input name="subject" id="subject" value="${message.subject}">
		</li>
		<li class="message-area">
			<label for="message">本文 (1000文字以下)</label>
			<textarea name="message" cols="60" rows="20" id="message">${message.message}</textarea>
		</li>
		<li class="category">
			<label for="category">カテゴリー (10文字以下)</label>
<%-- 			<input name="category" id="category" value="${message.category}"> --%>
			<input type="text" name="category" list="category" value="${message.category}" autocomplete="off"><br>
			<datalist id="category">
				<c:forEach items="${categorys}" var="categorys">
					<option value="${categorys}"></option>
				</c:forEach>
			</datalist>
		</li>
		<li>
			<input type="submit" value="投稿">
		</li>
	</ul>
</form>
</div>
<div class="copyright">Copyright(c)Hiroki Sanada</div>
</div>
<script type="text/javascript">
 $("form").submit(function() {
   var self = this;
   $(':submit', self).prop("disabled", true);
   setTimeout(function() {
     $(':submit', self).prop("disabled", false);
   }, 10000);
 });
</script>
</body>
</html>