package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Message;
import beans.User;
import service.MessageService;
import service.PostService;
/**
 * Servlet implementation class PostServlet
 */
@WebServlet("/post")
public class PostServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public PostServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		List<Message> allMessages = new ArrayList<Message>();
		List<String> categorys = new ArrayList<String>();

		allMessages = new MessageService().getMessage(null, null, null);
		categorys = CreateCategoryList(allMessages);

		request.setAttribute("categorys", categorys);
		request.getRequestDispatcher("post.jsp").forward(request, response);
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// 投稿ボタン押下時、入力内容をDBに登録してホーム画面に遷移する。
		// 投稿日時は現在日時を取得して更新日時とともに登録する。
		HttpSession session = request.getSession();
		List<String> errMessages = new ArrayList<String>();
		User user = (User) session.getAttribute("loginUser");
		Message message = new Message();

		message.setSubject(request.getParameter("subject"));
		message.setMessage(request.getParameter("message"));
		message.setCategory(request.getParameter("category"));
		message.setUserId(user.getId());

		if( IsValid(errMessages, message) ){
			new PostService().register(message);
			response.sendRedirect("./");
		} else {
			request.setAttribute("message", message);
			request.setAttribute("errMessages", errMessages);
			request.getRequestDispatcher("post.jsp").forward(request, response);
		}
	}

	private boolean IsValid(List<String> errMessages, Message message) {
		// 入力項目チェック
		int errFlag = 0;
		if( message.getSubject().length() == 0 || message.getSubject() == null ){
			errFlag = 1;
			errMessages.add("件名が入力されていません。");
		}
		if( message.getSubject().length() > 30 ){
			errFlag = 1;
			errMessages.add("件名が30字を超えています。");
		}
		if( message.getMessage().length() == 0 || message.getMessage() == null ){
			errFlag = 1;
			errMessages.add("本文が入力されていません。");
		}
		if( message.getMessage().length() > 1000 ){
			errFlag = 1;
			errMessages.add("本文が1000字を超えています。");
		}
		if( message.getCategory().length() == 0 || message.getCategory() == null ){
			errFlag = 1;
			errMessages.add("カテゴリーが入力されていません。");
		}
		if( message.getCategory().length() > 10 ){
			errFlag = 1;
			errMessages.add("カテゴリーが10字を超えています。");
		}
		if( errFlag == 0){
			return true;
		} else {
			return false;
		}
	}

	private List<String> CreateCategoryList(List<Message> allMessages) {
		// 全てのメッセージを取得、そこからカテゴリーをピックアップ
		List<String> categorys = new ArrayList<String>();
		for ( Message message : allMessages) {
			int flag = 0;
			String strCategroy = message.getCategory();
			for ( String c : categorys ){
				if ( c.equals(strCategroy) ){
					flag = 1;
					break;
				}
			}
			if( flag == 0 ){
				categorys.add(strCategroy);
			}
		}
		return categorys;
	}
}
