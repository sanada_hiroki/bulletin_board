package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Branch;
import beans.Department;
import beans.User;
import service.BranchService;
import service.DepartmentService;
import service.UserService;

/**
 * Servlet implementation class Ragister
 */
@WebServlet("/register")
public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegisterServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Branch> branches = new BranchService().getBranchList();
		List<Department> departments = new DepartmentService().getDepartmentList();

		request.setAttribute("branches", branches);
		request.setAttribute("departments", departments);
		request.getRequestDispatcher("register.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		List<String> errMessages = new ArrayList<String>();
		User user = new User();
		user.setLoginId(request.getParameter("loginid"));
		user.setPassword(request.getParameter("password"));
		user.setName(request.getParameter("name"));
		user.setBranchId(Integer.parseInt(request.getParameter("branch")));
		user.setDepartmentId(Integer.parseInt(request.getParameter("department")));
		String chkPassword = request.getParameter("checkPassword");

		if( IsValid(errMessages, user, chkPassword) ){
			new UserService().register(user);
			response.sendRedirect("management");
		} else {
			List<Branch> branches = new BranchService().getBranchList();
			List<Department> departments = new DepartmentService().getDepartmentList();

			request.setAttribute("branches", branches);
			request.setAttribute("departments", departments);
			request.setAttribute("errMessages", errMessages);
			request.setAttribute("user", user);
			request.getRequestDispatcher("register.jsp").forward(request, response);
		}
	}

	private boolean IsValid(List<String> errMessages, User user, String chkPassword) {
		// 入力チェック
		int errFlag = 0;
		if( user.getLoginId() == null || user.getLoginId().length() == 0){
			errMessages.add("ログインIDが入力されていません。");
			errFlag = 1;
		} else if( user.getLoginId().matches("^[0-9a-zA-Z]{6,20}$")  == false ){
			errMessages.add("ログインIDの入力値が正しくありません。");
			errFlag = 1;
		} else if( new UserService().getUser(0, user.getLoginId()) ){
			// ID重複チェック
			errMessages.add("すでに登録されたログインIDです。");
			errFlag = 1;
		}
		if( user.getName() == null || user.getName().length() == 0 ){
			errMessages.add("名前が入力されていません");
			errFlag = 1;
		} else if( user.getName().length() > 10 ){
			errMessages.add("名前の入力値が正しくありません。");
			errFlag = 1;
		}
		if( user.getPassword() == null || user.getPassword().length() == 0 ){
			errMessages.add("パスワードが入力されていません");
			errFlag = 1;
		} else if( user.getPassword().matches("^[0-9a-zA-Z_]{6,20}$") == false ){
			errMessages.add("パスワードの入力値が正しくありません。");
			errFlag = 1;
		}
		if( chkPassword == null || chkPassword.length() == 0 ){
			errMessages.add("確認用パスワードが入力されていません。");
			errFlag = 1;
		} else if( chkPassword.equals(user.getPassword()) == false ){
			errMessages.add("パスワードと確認用パスワードが一致していません。");
			errFlag = 1;
		}

		// 支店、部署・役職の整合性チェック
		if( user.getBranchId() == 1 ){
			if( user.getDepartmentId() == 3 || user.getDepartmentId() == 4 ){
				errMessages.add("支店と部署・役職の整合性が取れていません。");
				errFlag = 1;
			}
		} else {
			if( user.getDepartmentId() == 1 || user.getDepartmentId() == 2 ){
				errMessages.add("支店と部署・役職の整合性が取れていません。");
				errFlag = 1;
			}
		}
		if( errFlag == 0 ){
			return true;
		} else {
			return false;
		}
	}
}
