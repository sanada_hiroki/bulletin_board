package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Comment;
import beans.User;
import service.CommentService;

/**
 * Servlet implementation class CommentServlet
 */
@WebServlet("/comment")
public class CommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public CommentServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int commentId = Integer.parseInt(request.getParameter("id"));
		new CommentService().delete(commentId);
		response.sendRedirect("./");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		List<String> errMessages = new ArrayList<String>();
		User user = (User) session.getAttribute("loginUser");

		Comment comment = new Comment();
		comment.setMessage(request.getParameter("comment"));
		comment.setPostId(Integer.parseInt(request.getParameter("id")));
		comment.setUserId(user.getId());

		int errFlag = 0;
		if( comment.getMessage().length() < 1 ){
			errMessages.add("コメントが未入力です。");
			errFlag = 1;
		}
		if( comment.getMessage().length() > 500 ){
			errMessages.add("コメントが500字を超えています。");
			errFlag = 1;
		}
		if( errFlag == 0 ) {
			new CommentService().register(comment);
		} else {
			session.setAttribute("errMessages", errMessages);
			session.setAttribute("mycomment", comment);
		}

		response.sendRedirect("./");
	}
}
