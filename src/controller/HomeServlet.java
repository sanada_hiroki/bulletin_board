package controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Comment;
import beans.Message;
import service.CommentService;
import service.MessageService;

/**
 * Servlet implementation class HomeServlet
 */
@WebServlet(urlPatterns = { "/index.jsp" })
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public HomeServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		List<Message> messages = new ArrayList<Message>();
		List<Message> allMessages = new ArrayList<Message>();
		List<Comment> comments = new ArrayList<Comment>();
		List<String> categorys = new ArrayList<String>();

		String searchCategory = request.getParameter("category");
		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");
		int flag = 0;
		if ( startDate == null || startDate.length() == 0 ){
			flag = 1;
		}

		List<String> searchDate = new ArrayList<String>();
		searchDate = AdjustDate(startDate, endDate);
		startDate = searchDate.get(0);
		endDate = searchDate.get(1);

		messages = new MessageService().getMessage(startDate,endDate,searchCategory);
		allMessages = new MessageService().getMessage(null, null, null);
		comments = new CommentService().getComment();
		categorys = CreateCategoryList(allMessages);
		if ( messages == null || flag == 0 ) {
			startDate = startDate.substring(0, 10);
		} else {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			startDate = sdf.format(messages.get(messages.size() - 1).getCreatedAt());
		}

		request.setAttribute("startDate", startDate);
		request.setAttribute("endDate", endDate.substring(0, 10));
		request.setAttribute("searchCategory", searchCategory);
		request.setAttribute("messages", messages);
		request.setAttribute("comments", comments);
		request.setAttribute("categorys", categorys);
		request.setAttribute("retCode", "\r\n");
		request.getRequestDispatcher("home.jsp").forward(request, response);
	}

	private List<String> AdjustDate(String startDate, String endDate) {
		// 双方のnullチェック
		// 日付が前のほうをstartDate、後のほうをendDateに
		// startDateには" 00-00-00"、endDateには" 23-59-59"を追加する
		List<String> ret = new ArrayList<String>();
		if( startDate == null || startDate.length() == 0 ) {
			startDate = "2017-11-01";
		}
		if( endDate == null || endDate.length() == 0 ) {
			Date d = new Date();
			SimpleDateFormat d1 = new SimpleDateFormat("yyyy-MM-dd");
			endDate = d1.format(d);
		}
		if( startDate.compareTo(endDate) > 0) {
			String strDate = startDate;
			startDate = endDate;
			endDate = strDate;
		}

		startDate = startDate + " 00-00-00";
		endDate = endDate + " 23-59-59";
		ret.add(0, startDate);
		ret.add(1, endDate);
		return ret;
	}

	private List<String> CreateCategoryList(List<Message> allMessages) {
		// 全てのメッセージを取得、そこからカテゴリーをピックアップ
		List<String> categorys = new ArrayList<String>();
		for ( Message message : allMessages) {
			int flag = 0;
			String strCategroy = message.getCategory();
			for ( String c : categorys ){
				if ( c.equals(strCategroy) ){
					flag = 1;
					break;
				}
			}
			if( flag == 0 ){
				categorys.add(strCategroy);
			}
		}
		return categorys;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

}
