package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import service.LoginService;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet(urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("login.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();
		User user = new beans.User();
		session.removeAttribute("loginUser");

		if( IsValid(messages, loginId, password) ) {
			// アカウント存在チェック
			LoginService loginService = new LoginService();
			user = loginService.login(loginId, password);
			if ( user == null ) {
				messages.add("ログインに失敗しました。");
			} else {
				if( user.getIsBanned() == 1 ) {
					System.out.println("停止チェック");
					messages.add(user.getLoginId() + "は停止されたユーザーIDです。");
				}
			}
		}

		if( messages.size() != 0 ) {
			session.setAttribute("errMessages", messages);
			response.sendRedirect("login");
		} else {
			session.setAttribute("loginUser", user);
			response.sendRedirect("./");
		}
	}

	private boolean IsValid(List<String> messages, String loginId, String password) {

		// 入力値チェック
		if( loginId == null || loginId.length() == 0 ) {
			messages.add("ログインIDが入力されていません。");
		}
		if( password == null || password.length() == 0 ) {
			messages.add("パスワードが入力されていません。");
		}
		if( messages.size() != 0 ) {
			return false;
		} else {
			return true;
		}
	}
}
