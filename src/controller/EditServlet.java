package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Department;
import beans.User;
import service.BranchService;
import service.DepartmentService;
import service.UserService;

/**
 * Servlet implementation class EditServlet
 */
@WebServlet("/edit")
public class EditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String pram = request.getParameter("id");
		List<String> errMessages = new ArrayList<String>();
		HttpSession session = request.getSession();

		if( IsPramValid(pram) ){
			User user = new UserService().getUser(Integer.parseInt(pram));
			List<Branch> branches = new BranchService().getBranchList();
			List<Department> departments = new DepartmentService().getDepartmentList();
			request.setAttribute("branches", branches);
			request.setAttribute("departments", departments);
			request.setAttribute("user", user);
			request.getRequestDispatcher("edit.jsp").forward(request, response);
		} else {
			errMessages.add("不正な値が入力されました。");
			session.setAttribute("errMessages", errMessages);
			response.sendRedirect("./management");
		}
	}

	private boolean IsPramValid(String pram) {

		if( StringUtils.isEmpty(pram) ){
			return false;
		}
		if ( pram.matches("^\\d{1,9}$") == false ){
			return false;
		} else {
			int id = Integer.parseInt(pram);
			User user = new UserService().getUser(id);
			if ( user == null ){
				return false;
			}
		}
		return true;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		User editUser = getEditUser(request);
		String chkPassword = request.getParameter("checkPassword");
		List<String> errMessages = new ArrayList<String>();

		if( IsValid(errMessages, editUser, chkPassword) ){
			new UserService().update(editUser);
			response.sendRedirect("./management");
		} else {
			List<Branch> branches = new BranchService().getBranchList();
			List<Department> departments = new DepartmentService().getDepartmentList();

			request.setAttribute("branches", branches);
			request.setAttribute("departments", departments);
			request.setAttribute("errMessages", errMessages);
			request.setAttribute("user", editUser);
			request.getRequestDispatcher("edit.jsp").forward(request, response);
		}
	}

	private User getEditUser(HttpServletRequest request) {
		User user = new User();

		user.setId(Integer.parseInt(request.getParameter("id")));
		user.setLoginId(request.getParameter("loginId"));
		user.setName(request.getParameter("name"));
		user.setPassword(request.getParameter("password"));
		if ( request.getParameter("branch") == null ) {
			user.setBranchId(1);
			user.setDepartmentId(1);
		} else {
			user.setBranchId(Integer.parseInt(request.getParameter("branch")));
			user.setDepartmentId(Integer.parseInt(request.getParameter("department")));
		}
		return user;
	}

	private boolean IsValid(List<String> errMessages, User user, String chkPassword) {
		// 入力チェック
		int errFlag = 0;
		if( user.getLoginId() == null || user.getLoginId().length() == 0){
			errMessages.add("ログインIDが入力されていません。");
			errFlag = 1;
		} else if( user.getLoginId().matches("^[0-9a-zA-Z]{6,20}$")  == false ){
			errMessages.add("ログインIDの入力値が正しくありません。");
			errFlag = 1;
		} else if( new UserService().getUser(user.getId(), user.getLoginId()) ){
			// ID重複チェック
			errMessages.add("すでに登録されたログインIDです。");
			errFlag = 1;
		}
		if( user.getName() == null || user.getName().length() == 0 ){
			errMessages.add("名前が入力されていません");
			errFlag = 1;
		} else if( user.getName().length() > 10 ){
			errMessages.add("名前の入力値が正しくありません。");
			errFlag = 1;
		}
		if( user.getPassword().length() != 0 || chkPassword.length() != 0){
			if( user.getPassword().matches("^[0-9a-zA-Z_]{6,20}$") == false ){
				errMessages.add("パスワードの入力値が正しくありません。");
				errFlag = 1;
			}
			if( chkPassword.equals(user.getPassword()) == false ){
				errMessages.add("パスワードと確認用パスワードが一致していません。");
				errFlag = 1;
			}
		}
		// 支店、部署・役職の整合性チェック
		if( user.getBranchId() == 1 ){
			if( user.getDepartmentId() == 3 || user.getDepartmentId() == 4 ){
				errMessages.add("支店と部署・役職の整合性が取れていません。");
				errFlag = 1;
			}
		} else {
			if( user.getDepartmentId() == 1 || user.getDepartmentId() == 2 ){
				errMessages.add("支店と部署・役職の整合性が取れていません。");
				errFlag = 1;
			}
		}
		if( errFlag == 0 ){
			return true;
		} else {
			return false;
		}
	}
}
