package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import beans.Message;
import exception.SQLRuntimeException;

public class MessageDao {

	public void insert(Connection connection, Message message) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO posts ( ");
			sql.append("subject");
			sql.append(", message");
			sql.append(", category");
			sql.append(", created_at");
			sql.append(", user_id");
			sql.append(") VALUES (");
			sql.append("?"); // subject
			sql.append(", ?"); // message
			sql.append(", ?"); // category
			sql.append(", CURRENT_TIMESTAMP"); // created_at
			sql.append(" , ?"); // user_id
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, message.getSubject());
			ps.setString(2, message.getMessage());
			ps.setString(3, message.getCategory());
			ps.setInt(4, message.getUserId());

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public List<Message> getMessages(Connection connection, String startDate, String endDate, String searchCategory) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT " );
			sql.append(" posts.id, ");
			sql.append(" subject, ");
			sql.append(" message, ");
			sql.append(" category, ");
			sql.append(" posts.created_at, ");
			sql.append(" user_id, ");
			sql.append(" users.name ");
			sql.append("FROM posts INNER JOIN users ");
			sql.append(" ON posts.user_id = users.id ");
			sql.append("WHERE ( posts.created_at BETWEEN ? AND ? ) ");
			if(searchCategory != null && searchCategory.length() != 0 ){
				sql.append("AND category LIKE ? ");
			}
			sql.append(" ORDER BY posts.created_at DESC ");

			ps = connection.prepareStatement(sql.toString());

			if( startDate == null ){
				startDate = "2017-11-01 00-00-00";
			}
			if( endDate == null ){
				Date d = new Date();
				SimpleDateFormat d1 = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
				endDate = d1.format(d);
			}

			ps.setString(1, startDate);
			ps.setString(2, endDate);
			if( searchCategory != null && searchCategory.length() != 0 ){
				ps.setString(3, "%" + searchCategory + "%");
			}

			ResultSet rs = ps.executeQuery();
			List<Message> messages = toMessageList(rs);
			if (messages.isEmpty() == true) {
				return null;
			} else {
				return messages;
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<Message> toMessageList(ResultSet rs) {
		List<Message> ret = new ArrayList<Message>();
		try {
			while (rs.next()) {
				int id = rs.getInt("posts.id");
				String subject = rs.getString("subject");
				String message = rs.getString("message");
				String category = rs.getString("category");
				Timestamp createdAt = rs.getTimestamp("posts.created_at");
				int userId = rs.getInt("user_id");
				String name = rs.getString("name");

				Message messages = new Message();
				messages.setId(id);
				messages.setSubject(subject);
				messages.setMessage(message);
				messages.setCategory(category);
				messages.setCreatedAt(createdAt);
				messages.setUserId(userId);
				messages.setUserName(name);

				ret.add(messages);
			}
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(rs);
		}
	}

	public void delete(Connection connection, int msgId) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("DELETE FROM posts ");
			sql.append(" WHERE id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, msgId);

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

}
