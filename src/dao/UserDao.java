package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.ManagementUser;
import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import exception.SQLRuntimeException;

public class UserDao {

	public User getUser(Connection connection, String loginId,
			String password) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT " );
			sql.append(" users.id, ");
			sql.append(" users.login_id, ");
			sql.append(" users.password, ");
			sql.append(" users.name, ");
			sql.append(" users.branch_id, ");
			sql.append(" branches.name, ");
			sql.append(" users.department_id, ");
			sql.append(" departments.name, ");
			sql.append(" users.is_banned, ");
			sql.append(" users.created_at, ");
			sql.append(" users.updated_at ");
			sql.append("FROM users ");
			sql.append("INNER JOIN branches ON branch_id = branches.id  ");
			sql.append("INNER JOIN departments ON department_id = departments.id  ");
			sql.append("WHERE users.login_id = ? ");
			sql.append(" AND  users.password = ? ");

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, loginId);
			ps.setString(2, password);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if (userList.isEmpty() == true) {
				return null;
			} else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<User> toUserList(ResultSet rs) throws SQLException {

		List<User> ret = new ArrayList<User>();
		try {
			while (rs.next()) {
				int id = rs.getInt("users.id");
				String loginId = rs.getString("users.login_id");
				String password = rs.getString("users.password");
				String name = rs.getString("users.name");
				int branchId = rs.getInt("users.branch_id");
				String branchName = rs.getString("branches.name");
				int departmentId = rs.getInt("users.department_id");
				String departmentName = rs.getString("departments.name");
				int isBanned = rs.getInt("users.is_banned");
				Timestamp createdAt = rs.getTimestamp("users.created_at");
				Timestamp updatedAt = rs.getTimestamp("users.updated_at");

				User user = new User();
				user.setId(id);
				user.setLoginId(loginId);
				user.setPassword(password);
				user.setName(name);
				user.setBranchId(branchId);
				user.setBranchName(branchName);
				user.setDepartmentId(departmentId);
				user.setDepartmentName(departmentName);
				user.setIsBanned(isBanned);
				user.setCreatedAt(createdAt);
				user.setUpdatedAt(updatedAt);

				ret.add(user);
			}
			return ret;
		} finally {
			close(rs);
		}
	}


	public void insert(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO users ( ");
			sql.append("login_id");
			sql.append(", password");
			sql.append(", name");
			sql.append(", branch_id");
			sql.append(", department_id");
			sql.append(", created_at");
			sql.append(", updated_at");
			sql.append(") VALUES (");
			sql.append("?"); // login_id
			sql.append(", ?"); // password
			sql.append(", ?"); // name
			sql.append(", ?"); // branch_id
			sql.append(", ?"); // department_id
			sql.append(", CURRENT_TIMESTAMP"); // created_at
			sql.append(", CURRENT_TIMESTAMP"); // updated_at
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getLoginId());
			ps.setString(2, user.getPassword());
			ps.setString(3, user.getName());
			ps.setInt(4, user.getBranchId());
			ps.setInt(5, user.getDepartmentId());

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public List<ManagementUser> getAllUsers(Connection connection) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT " );
			sql.append(" users.id, ");
			sql.append(" users.login_id, ");
			sql.append(" users.name, ");
			sql.append(" users.is_banned, ");
			sql.append(" branches.name, ");
			sql.append(" departments.name ");
			sql.append("FROM users INNER JOIN branches ");
			sql.append(" ON users.branch_id = branches.id ");
			sql.append(" INNER JOIN departments ");
			sql.append(" ON users.department_id = departments.id ");
			sql.append(" ORDER BY users.branch_id, users.department_id ");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<ManagementUser> userList = toAllUsers(rs);
			if (userList.isEmpty() == true) {
				return null;
			} else {
				return userList;
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<ManagementUser> toAllUsers(ResultSet rs) throws SQLException {

		List<ManagementUser> ret = new ArrayList<ManagementUser>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("users.login_id");
				String name = rs.getString("users.name");
				String branchName = rs.getString("branches.name");
				String departmentName = rs.getString("departments.name");
				int isBanned = rs.getInt("is_banned");

				ManagementUser user = new ManagementUser();
				user.setId(id);
				user.setLoginId(loginId);
				user.setName(name);
				user.setBranchName(branchName);
				user.setDepartmentName(departmentName);
				user.setIsBanned(isBanned);

				ret.add(user);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	public void update(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append("  login_id = ?");
			sql.append(", name = ?");
			sql.append(", branch_id = ?");
			sql.append(", department_id = ?");
			sql.append(", updated_at = CURRENT_TIMESTAMP");
			if( user.getPassword().length() != 0 ){
				sql.append(", password = ?");
			}
			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, user.getLoginId());
			ps.setString(2, user.getName());
			ps.setInt(3, user.getBranchId());
			ps.setInt(4, user.getDepartmentId());

			if( user.getPassword().length() != 0 ){
				ps.setString(5, user.getPassword());
				ps.setInt(6, user.getId());
			} else {
				ps.setInt(5, user.getId());
			}

			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}

	public User getUser(Connection connection, int id) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT " );
			sql.append(" users.id, ");
			sql.append(" users.login_id, ");
			sql.append(" users.password, ");
			sql.append(" users.name, ");
			sql.append(" users.branch_id, ");
			sql.append(" branches.name, ");
			sql.append(" users.department_id, ");
			sql.append(" departments.name, ");
			sql.append(" users.is_banned, ");
			sql.append(" users.created_at, ");
			sql.append(" users.updated_at ");
			sql.append("FROM users ");
			sql.append("INNER JOIN branches ON branch_id = branches.id  ");
			sql.append("INNER JOIN departments ON department_id = departments.id  ");
			sql.append("WHERE users.id = ? ");

			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1, id);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if (userList.isEmpty() == true) {
				return null;
			} else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public void update(Connection connection, int userId, Boolean isBanned) {
		// TODO 自動生成されたメソッド・スタブ
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append("  is_banned = ?");
			sql.append(", updated_at = CURRENT_TIMESTAMP");
			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setBoolean(1, isBanned);
			ps.setInt(2, userId);

			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public Boolean getUser(Connection connection, int id, String loginId) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM users WHERE login_id = ? ");
			if( id != 0 ){
				sql.append(" AND id != ?" );
			}

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, loginId);
			if( id != 0 ) {
				ps.setInt(2, id);
			}

			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				return true;
			} else {
				return false;
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
}
