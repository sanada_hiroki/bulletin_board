package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.Comment;
import exception.SQLRuntimeException;

public class CommentDao {

	public void insert(Connection connection, Comment comment) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO comments ( ");
			sql.append("message");
			sql.append(", created_at");
			sql.append(", user_id");
			sql.append(", post_id");
			sql.append(") VALUES (");
			sql.append("?"); // message
			sql.append(", CURRENT_TIMESTAMP"); // created_at
			sql.append(", ?"); // user_id
			sql.append(", ?"); // post_id
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, comment.getMessage());
			ps.setInt(2, comment.getUserId());
			ps.setInt(3, comment.getPostId());

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public List<Comment> getComment(Connection connection) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT " );
			sql.append(" comments.id, ");
			sql.append(" comments.message, ");
			sql.append(" comments.post_id, ");
			sql.append(" comments.created_at, ");
			sql.append(" comments.user_id, ");
			sql.append(" users.name ");
			sql.append("FROM comments INNER JOIN users ");
			sql.append(" ON comments.user_id = users.id ");
			sql.append(" ORDER BY comments.created_at DESC ");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<Comment> comments = toCommentList(rs);
			if (comments.isEmpty() == true) {
				return null;
			} else {
				return comments;
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<Comment> toCommentList(ResultSet rs) {
		List<Comment> ret = new ArrayList<Comment>();
		try {
			while (rs.next()) {
				int id = rs.getInt("comments.id");
				int postId = rs.getInt("comments.post_id");
				String message = rs.getString("comments.message");
				Timestamp createdAt = rs.getTimestamp("comments.created_at");
				int userId = rs.getInt("comments.user_id");
				String name = rs.getString("users.name");

				Comment comments = new Comment();
				comments.setId(id);
				comments.setPostId(postId);
				comments.setMessage(message);
				comments.setCreatedAt(createdAt);
				comments.setUserId(userId);
				comments.setUserName(name);

				ret.add(comments);
			}
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(rs);
		}
	}

	public void delete(Connection connection, int commentId) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("DELETE FROM comments ");
			sql.append(" WHERE id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, commentId);

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

}
