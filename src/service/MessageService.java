package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Message;
import dao.MessageDao;

public class MessageService {

	public List<Message> getMessage(String startDate, String endDate, String searchCategory) {

		Connection connection = null;
		try {
			connection = getConnection();

			List<Message> ret = new MessageDao().getMessages(connection, startDate, endDate, searchCategory);
			commit(connection);
			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void delete(int msgId) {
		Connection connection = null;
		try {
			connection = getConnection();

			MessageDao MessageDao = new MessageDao();
			MessageDao.delete(connection, msgId);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}
