package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import service.UserService;

/**
 * Servlet Filter implementation class LoginFilter
 */
@WebFilter(urlPatterns = {"/authority","/comment","/delete","/edit","/index.jsp","/management","/post","/register"})
public class LoginFilter implements Filter {

    /**
     * Default constructor.
     */
    public LoginFilter() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		// TODO Auto-generated method stub
		// place your code here
		String strPass = ((HttpServletRequest)request).getServletPath();
		HttpSession session = ((HttpServletRequest)request).getSession();
		User loginUser = (User) session.getAttribute("loginUser");
		List<String> messages = new ArrayList<String>();

		if (loginUser == null ){
			/* まだログインしていない */
			if( strPass.equals("/login") == false ) {
				messages.add("掲示板システムはログイン後に利用できます。");
				session.setAttribute("errMessages", messages);
				((HttpServletResponse)response).sendRedirect("login");
				return;
			}
		} else {
			// セッションのloginUserを最新に更新する
			int userId = loginUser.getId();
			loginUser = new UserService().getUser(userId);
			session.setAttribute("loginUser", loginUser);

			// 編集権限のチェック
			int generalId = 1;
			int departmentId = loginUser.getDepartmentId();
			if( departmentId != generalId ){
				if( strPass.equals("/management")  ){
					messages.add("ユーザー管理画面へのアクセス権限がありません。");
				} else if ( strPass.equals("/register") ){
					messages.add("ユーザー新規登録画面へのアクセス権限がありません。");
				} else if ( strPass.equals("/edit") ){
					messages.add("ユーザー編集画面へのアクセス権限がありません。");
				}
			}

			if( messages.size() != 0 ) {
				session.setAttribute("errMessages", messages);
				((HttpServletResponse)response).sendRedirect("./");
				return;
			}

			if( loginUser.getIsBanned() == 1 ) {
				messages.add(loginUser.getLoginId() + "は停止されたユーザーIDです。");
				session.setAttribute("errMessages", messages);
				((HttpServletResponse)response).sendRedirect("login");
				return;
			}
		}
		// pass the request along the filter chain
		chain.doFilter(request, response);
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
