package beans;

import java.io.Serializable;

public class ManagementUser implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;
	private String loginId;
	private String name;
	private String branchName;
	private String departmentName;
	private int isBanned;

	public int getIsBanned() {
		return isBanned;
	}

	public void setIsBanned(int isBanned) {
		this.isBanned = isBanned;
	}

	public int getId(){
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getName(){
		return name;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName( String branchName ) {
		this.branchName = branchName;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName( String departmentName ) {
		this.departmentName = departmentName;
	}
}
